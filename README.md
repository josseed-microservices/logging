# Logging microservice
This project allows logging by the hashtag for different software.

- Java 11
- Spring boot
- Mysql

# For Developers

Is necessary to have installed Java, Docker and Docker-compose to follow this steps.
Is recommendable to use IntelliJ as IDE.

To build the database we will use the docker-compose.yml file.
Which contains a MariaDB database and PhpMyAdmin as the db viewer, it also has the credentials of these technologies.

 ```
 docker-compose up -d
 ```
Mysql DB uses the port 3306.

PhpMyAdmin use the port 7300.

For installing the packages of mvn 

```
mvn install
```

You can run the project in intelliJ and access to:

http://localhost:8080/swagger-ui/

To see the endpoints available