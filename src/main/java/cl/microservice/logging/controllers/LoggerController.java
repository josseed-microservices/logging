package cl.microservice.logging.controllers;
import cl.microservice.logging.entities.Logger;
import cl.microservice.logging.services.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/logs")
public class LoggerController {

    @Autowired
    private LoggerService loggerService;

    /**
     * Get all Logs
     * @return ResponseEntity
     */
    @GetMapping
    public ResponseEntity<List<Logger>> getAllLogs() {
        return ResponseEntity.ok(this.loggerService.getAllLoggers());
    }

    /**
     * Insert a new log, creating the tags if not exists.
     * @param logger
     * @return ResponseEntity
     */
    @PostMapping
    public ResponseEntity<Logger> createLogger(@Valid @RequestBody Logger logger) {
        return  ResponseEntity.status(201).body(this.loggerService.createLogger(logger));
    }

    /**
     * get a Logger instance by ID
     * @param loggerId
     * @return ResponseEntity
     */
    @GetMapping("/{id}")
    public ResponseEntity<Logger> getLoggerById(@PathVariable ("id") Long loggerId) {
        return ResponseEntity.ok(this.loggerService.getLoggerById(loggerId));
    }

    /**
     * get a list of loggers by hashtag
     * @param hashtag
     * @return ResponseEntity
     */
    @GetMapping("/hashtag/{hashtag}")
    public ResponseEntity<List<Logger>> getLoggersByHashTag(@PathVariable ("hashtag") String hashtag) {
        return ResponseEntity.ok(this.loggerService.getLoggersByHashtag(hashtag));
    }
}
