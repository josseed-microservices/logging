package cl.microservice.logging.controllers;

import cl.microservice.logging.entities.Hashtag;
import cl.microservice.logging.services.HashTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hashtags")
public class HashtagController {

    @Autowired
    private HashTagService hashTagService;

    /**
     * update the description of a hashtag by id.
     * @param hashTag Hashtag instance
     * @return ResponseEntity
     */
    @PutMapping
    public ResponseEntity<Hashtag> updateHashTag(@RequestBody Hashtag hashTag) {
        return ResponseEntity.ok(this.hashTagService.updateHashTag(hashTag));
    }
}
