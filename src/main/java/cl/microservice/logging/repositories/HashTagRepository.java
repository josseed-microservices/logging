package cl.microservice.logging.repositories;

import cl.microservice.logging.entities.Hashtag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HashTagRepository extends JpaRepository<Hashtag, Long> {

    Optional<Hashtag> findByDescription(String description);
}
