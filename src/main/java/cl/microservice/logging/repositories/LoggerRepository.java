package cl.microservice.logging.repositories;

import cl.microservice.logging.entities.Logger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoggerRepository extends JpaRepository<Logger, Long> {
    @Query(
        value="select l.* from logger as l" +
            " JOIN logger_hashtags lh on l.id = lh.loggers_id " +
            "JOIN hashtag h on lh.hashtags_id = h.id " +
            "WHERE LOWER(h.description) like %:hashtag%",
        nativeQuery = true
    )
    List<Logger> findLoggersByHashTag(String hashtag);
}
