package cl.microservice.logging.services;

import cl.microservice.logging.entities.Hashtag;

public interface HashTagService {

    /**
     * @param hashtag instance with a different and unique description for update.
     * @return hashtag instance updated.
     */
    Hashtag updateHashTag(Hashtag hashtag);
}
