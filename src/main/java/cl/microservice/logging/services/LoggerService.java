package cl.microservice.logging.services;

import cl.microservice.logging.entities.Logger;

import java.util.List;
import java.util.Set;

public interface LoggerService {

    /**
     * @return an array containing all the logger elements
     */
    List<Logger> getAllLoggers();

    /**
     * @param loggerId id of a logger
     * @return a specific logger by id
     */
    Logger getLoggerById(Long loggerId);

    /**
     * @param logger instance of Logger without id.
     * @return logger instance with the id created.
     */
    Logger createLogger(Logger logger);

    /**
     * @param hashtag for looking all the logs created with this.
     * @return an array containing all the logger elements with that hashtag.
     */
    List<Logger> getLoggersByHashtag(String hashtag);
}
