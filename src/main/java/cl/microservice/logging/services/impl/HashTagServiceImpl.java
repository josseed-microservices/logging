package cl.microservice.logging.services.impl;

import cl.microservice.logging.entities.Hashtag;
import cl.microservice.logging.exceptions.BadRequestException;
import cl.microservice.logging.exceptions.ConflictException;
import cl.microservice.logging.exceptions.ResourceNotFoundException;
import cl.microservice.logging.repositories.HashTagRepository;
import cl.microservice.logging.services.HashTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HashTagServiceImpl implements HashTagService {

    @Autowired
    private HashTagRepository hashTagRepository;

    /**
     * @param hashtag instance with a different and unique description for update.
     * @return hashtag instance updated.
     */
    @Override
    public Hashtag updateHashTag(Hashtag hashtag) {

        if (hashtag.getId() == null) {
            throw new BadRequestException("id is required for updating a hashtag.");
        }
        if (!this.hashTagRepository.findById(hashtag.getId()).isPresent()) {
            throw new ResourceNotFoundException("Hashtag not found with the id:" + hashtag.getId());
        }
        Hashtag oldHashTag = this.hashTagRepository.findById(hashtag.getId()).get();
        if (oldHashTag.getDescription().equalsIgnoreCase(hashtag.getDescription())) {
            return hashtag;
        }

        if (this.hashTagRepository.findByDescription(hashtag.getDescription()).isPresent()) {
            throw new ConflictException("this hashtag already exists.");
        }

        return this.hashTagRepository.save(hashtag);
    }
}
