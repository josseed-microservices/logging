package cl.microservice.logging.services.impl;

import cl.microservice.logging.exceptions.BadRequestException;
import cl.microservice.logging.exceptions.ResourceNotFoundException;
import cl.microservice.logging.entities.Hashtag;
import cl.microservice.logging.entities.Logger;
import cl.microservice.logging.repositories.HashTagRepository;
import cl.microservice.logging.repositories.LoggerRepository;
import cl.microservice.logging.services.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LoggerServiceImpl implements LoggerService {

    @Autowired
    private LoggerRepository loggerRepository;

    @Autowired
    private HashTagRepository hashTagRepository;

    /**
     * @return an array containing all the logger elements
     */
    @Override
    public List<Logger> getAllLoggers() {
        return this.loggerRepository.findAll();
    }

    /**
     * @param loggerId id of a logger
     * @return a specific logger by id
     */
    @Override
    public Logger getLoggerById(Long loggerId) {
        return this.loggerRepository.findById(loggerId)
                .orElseThrow(() -> new ResourceNotFoundException("Logger not found with id :" + loggerId));
    }

    /**
     * @param logger instance of Logger without id.
     * @return logger instance with the id created.
     */
    @Override
    @Transactional
    public Logger createLogger(Logger logger) {
        Set<Hashtag> hashtags = logger.getHashtags();
        if (hashtags == null || hashtags.isEmpty()) {
            throw new BadRequestException("Hashtags is required for creating a log.");
        }
        Hashtag tempHashtag;
        Set<Hashtag> hashtagsRelated = new HashSet<>();
        for (Hashtag hashTag: hashtags) {
            if (!this.hashTagRepository.findByDescription(hashTag.getDescription()).isPresent()) {
                tempHashtag = hashTagRepository.save(hashTag);
            } else {
                tempHashtag = this.hashTagRepository.findByDescription(hashTag.getDescription()).get();
            }
            hashtagsRelated.add(tempHashtag);
        }
        logger.setHashtags(hashtagsRelated);
        return this.loggerRepository.save(logger);
    }

    /**
     * @param hashtag for looking all the logs created with this.
     * @return an array containing all the logger elements with that hashtag.
     */
    @Override
    public List<Logger> getLoggersByHashtag(String hashtag) {
        return this.loggerRepository.findLoggersByHashTag(hashtag.toLowerCase());
    }
}
