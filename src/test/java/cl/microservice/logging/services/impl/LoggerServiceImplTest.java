package cl.microservice.logging.services.impl;

import cl.microservice.logging.entities.Hashtag;
import cl.microservice.logging.entities.Logger;
import cl.microservice.logging.exceptions.BadRequestException;
import cl.microservice.logging.repositories.HashTagRepository;
import cl.microservice.logging.repositories.LoggerRepository;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;


@SpringBootTest
public class LoggerServiceImplTest {

    @Mock
    private HashTagRepository hashTagRepository;

    @Mock
    private LoggerRepository loggerRepository;

    @InjectMocks
    private LoggerServiceImpl loggerService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAllLoggersWhenNoRecord() {
        when(loggerRepository.findAll()).thenReturn(Arrays.asList());
        assertThat(loggerService.getAllLoggers().size(), is(0));
        Mockito.verify(loggerRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void findAllLoggersWithRecords() {
        when(loggerRepository.findAll()).thenReturn(Arrays.asList(new Logger()));
        assertThat(loggerService.getAllLoggers().size(), is(1));
        Mockito.verify(loggerRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void findLoggerById() {
        Logger logger = new Logger();
        when(loggerRepository.findById((long) 1)).thenReturn(java.util.Optional.of(logger));
        assertThat(loggerService.getLoggerById((long) 1), is(logger));
    }

    @Test
    public void createLogger() {
        Logger logger = new Logger();
        Set<Hashtag> hashtags = new HashSet<>();
        hashtags.add(new Hashtag());
        logger.setHashtags(hashtags);
        when(loggerRepository.save(logger)).thenReturn(logger);
        Logger newLogger = loggerService.createLogger(logger);
        assertThat(newLogger, is(logger));
    }

    @Test
    public void createInvalidLogger() {
        Logger logger = new Logger();
        when(loggerRepository.save(logger)).thenThrow(new BadRequestException(""));
        BadRequestException response = Assertions.assertThrows(BadRequestException.class, () -> {
            loggerService.createLogger(logger);
        });
        assertThat(response.getMessage()).isEqualTo("Hashtags is required for creating a log.");
    }

    @Test
    public void getLogsByHashtag() {
        when(loggerRepository.findLoggersByHashTag(Mockito.anyString())).thenReturn(Arrays.asList(new Logger()));
        assertThat(loggerService.getLoggersByHashtag(Mockito.anyString()).size(), is(1));
    }

}
