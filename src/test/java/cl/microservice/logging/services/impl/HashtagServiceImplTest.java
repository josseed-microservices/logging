package cl.microservice.logging.services.impl;

import cl.microservice.logging.entities.Hashtag;
import cl.microservice.logging.entities.Logger;
import cl.microservice.logging.repositories.HashTagRepository;
import cl.microservice.logging.services.impl.HashTagServiceImpl;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class HashtagServiceImplTest {

    @Mock
    private HashTagRepository hashTagRepository;

    @InjectMocks
    private HashTagServiceImpl hashTagService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAllHashTagWhenNoRecord() {
        Hashtag hashtag = new Hashtag();
        hashtag.setId((long) 1);
        hashtag.setDescription("hola");
        Mockito.when(hashTagRepository.findById((long) 1)).thenReturn(java.util.Optional.of(hashtag));
        Hashtag newHashtag = hashTagService.updateHashTag(hashtag);
        assertThat(newHashtag).isEqualTo(hashtag);
    }
}
